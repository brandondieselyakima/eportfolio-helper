import React from "react";
import { useNavigate } from "react-router-dom";
import Button from "../Button/Button";
import { FaRegEnvelope } from "react-icons/fa";


import "./header.css";


const Header = () => {
    const navigate = useNavigate();

    const handleClick = () => {
        navigate("/eportfolio-helper");
    }

    return (

        <div className="header-container">

            <div className="logo-container" onClick={handleClick}>
                <div className="logo2"></div>
            </div>

            <Button
                icon={<FaRegEnvelope />}
                text="Contact Us"
            />

        </div>
    )
};

export default Header;