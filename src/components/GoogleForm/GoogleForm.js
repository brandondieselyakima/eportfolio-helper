import React from "react";

const GoogleForm = () => {
    return (
        <iframe
            src="https://docs.google.com/forms/d/e/1FAIpQLSeUg2Tud6zYQqiK5F2wnHXANag5ZZ0CTPo5IO7WS1VQu_wvSg/viewform?embedded=true"
            width="1240"
            height="1477"
            frameborder="0"
            marginheight="0"
            marginwidth="0"
            title="Contact Us">Loading…</iframe>
    );
}

export default GoogleForm;
