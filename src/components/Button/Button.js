import React from "react";
import { useNavigate } from "react-router-dom";

import "./Button.css";

const Button = (props) => {
    const {
        icon,
        style,
        text,
    } = props;

    const navigate = useNavigate();

    const handleClick = () => {
        navigate("/contact");
    }
    return (
        <div className="button" onClick={handleClick} style={style}>
            <div className="button-text">{text}</div>
            <div className="button-icon">{icon}</div>
        </div>
    )
}

export default Button;
