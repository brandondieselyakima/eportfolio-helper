import React from "react";
import { useNavigate } from "react-router-dom";
import Searchbox from '../Searchbox/Searchbox';
import "./NavigationBar.css"
import { FaSearch } from "react-icons/fa";


const NavigationBar = (props) => {
    const { style } = props;
    const navigate = useNavigate();

    const handleClick = () => {
        navigate("/portfolio")
    }

    const handleClick2 = () => {
        navigate("/OurTeam")
    }

    const handleClick3 = () => {
        navigate("/HowToBuildPortfolio")
    }

    const handleClick4 = () => {
        navigate("/Examples")
    }

    return (
        <div className="navbar-container" style={style}>
            <div className="tab-container">
                <div className="tab"
                    onClick={handleClick}>
                    WHAT'S E-PORTFOLIO?</div>
                <div className="tab"
                    onClick={handleClick3}>HOW TO BUILD PORTFOLIO</div>
                <div className="tab"
                    onClick={handleClick4}>EXAMPLES</div>
                <div className="tab"
                    onClick={handleClick2}> OUR TEAM</div>
                <Searchbox icon={<FaSearch />} />
            </div>
        </div>
    )
};

export default NavigationBar;
