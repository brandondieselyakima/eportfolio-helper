import React from "react";
import Header from "../Header/header"
import NavigationBar from "../Navigation Bar/NavigationBar";
import PageBody from "../Page Body/PageBody";
import Footer from "../Footer/Footer";

const OurTeam = () => {
    return (
        <div className="OurTeam-container">
            <Header />
            <NavigationBar style={{ background: "#E9CCB7" }} />
            <PageBody style={{ backgroundImage: "none" }}
                portraitContainer
                imgContainer1
                imgContainer2
                imgContainer3
                imgContainer4
                sectionTitle2="OUR TEAM"
                portraitImage1
                portraitImage2
                portraitImage3
                portraitImage4

                name1="Anna Sabin"
                name2="Brandin Armenta"
                name3="Jorge Magana"
                name4="Dante Schoeller"
            />
            <Footer />
        </div>
    )
}

export default OurTeam;