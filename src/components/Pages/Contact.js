import React from "react";
import Header from "../Header/header"
import NavigationBar from "../Navigation Bar/NavigationBar";
import PageBody from "../Page Body/PageBody";
import Footer from "../Footer/Footer";

const Contact = () => {
    return (
        <div className="contact-container">
            <Header />
            <NavigationBar style={{ background: "#E9CCB7" }} />
            <PageBody style={{ backgroundImage: "none" }}
                form />
            <Footer />
        </div>
    )
}

export default Contact;