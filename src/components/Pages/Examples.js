import React from "react";
import Header from "../Header/header"
import NavigationBar from "../Navigation Bar/NavigationBar";
import PageBody from "../Page Body/PageBody";
import Footer from "../Footer/Footer";

const Examples = () => {
    return (
        <div className="examples-page">
            <Header />
            <NavigationBar style={{ background: "#E9CCB7" }} />
            <PageBody style={{ backgroundImage: "none" }}
                examplesBodyContainer
                examplesContent
                TitleExamples="EXAMPLES"
                pageContent13="E-portfolio is a collection of accomplishments, skills, experiences, and attributes, which can represent a method of self-discovery and confidence building. It serves as a proof of your skills and abilities. 
                            Many hiring managers require an e-portfolio from their applicants. An e-portfolio includes different components, that is resume, certifications, degree, awards, relevant work sample, hobbies, something that describes the personality, etc. However, your e-portfolio should be adjusted for the desired career position. 
                            "
                pageContent14="Below provided examples on how to improve your e-portfolio depending on chosen career field."
                examplesBottomContainer
                examplesLeftContainer
                examplesRightContainer
                exampleMainPicture
                portfolioImage2
                portfolioPictureBox1
                portfolioPictureBox2
                portfolioPictureBox3
                portfolioPictureBox4
                portfolioPictureBox5
                portfolioPictureBox6
                portfolioPictureBox7
                portfolioPictureBox8
                portfolioPictureBox9
                portfolioPictureBox10
                portfolioPictureBox11
                UXTitle="UX DESIGNER E-PORTFOLIO"
                pageContent15="E-portfolio is very important if you are planning to pursue a career of a UX designer. Most of the employers require e-portfolio for the UX designer position. 
                            Designer is a person who plans a form or a structure of something that is not made yet. The design can be related to visual and logical structure.
                             Therefore, the work of UX designer includes requirement description, analytics, problem solving, and visual representation. 
                            The most important is to outline your user-centered design process and to show how the problem was solved."
                pageContent16="The hiring managers in general want to see the process from the beginning to the end, including what issues occurred and how were they solved, constraints, project timeline, changes during the project.
                 Hiring managers want to see user research, research reports, 
                sketches, wireframes, user flows, user stories, customer journey maps, prototypes, user-testing, and the final product. It is beneficial to include analytics tools that were used for measuring the product success."
                pageContent18="Building content of your project samples for your portfolio ask yourself two questions: Are you proud of this work? Do you want to do a project like this again? If your answer “no” to either of these questions, you should not include the work in the portfolio."
                pageContent19="Also, your portfolio is a opportunity to show your personality. Nowadays, many companies make an emphasis on hiring people who match the culture of the business and a portfolio is a perfect opportunity to show that you fit the company culture."
                pageContent20="Remember, even if your projects were not successful or it was a part of your UX design course instead of real job project, do not afraid to show it. Work process, determination and motivation are very important.
                 Unless you are experienced UX designer, many hiring managers are not expecting to see perfect-driven work and they want to see your personality."

                portfolioAnna1
                portfolioAnna2
                portfolioAnna3
                portfoliojm
                portfoliojm1
                portfoliojm2
                portfolioBA
                portfolioBA1
                portfolioBA2
                portfolioDS1
                portfolioDS2


                bulletedListBox3
                sectionTitle3="What to avoid:"
                bulletedListItemEx1="Technical jargon"
                bulletedListItemEx2="Very colorful and splashy imagery"
                bulletedListItemEx3="Too much text"
                bulletedListItemEx4="Unclear project summary"

                bulletedListBox4
                sectionTitle4="For your UX-design e-portfolio it is beneficial to create a case study that includes following information:  "
                bulletedListItemEx5="The problems that you solved or hypothesis you came up with to solve it."
                bulletedListItemEx6="Specific role in the project and how did you collaborate with others."
                bulletedListItemEx7="How did your proposed solutions solved the problem."
                bulletedListItemEx8="Challenges you faces, including design concepts that eventually did not pursued."
                bulletedListItemEx9="What did you learn."

                engineerTitle="SOFTWARE ENGINEER E-PORTFOLIO"
                pageContent21="The projects section of your E-Portfolio is probably one of the most important for software engineers.
                here you get the chance to showcase all the work you're the most proud of. Make sure you explain your projects in the most
                short but detailed way; include the languages used for your projects, the tools utilized, and show a preview of your work."

                pageContent22="Don't forget to add all the languages you are proficient on and even those you know even a little of. 
                If you're a self-taught, add any certification and courses completed."

                qatTitle="QUALITY ASSURANCE TESTER E-PORTFOLIO"
                pageContent23="A Quality Assurance Tester is someone who tests software products to ensure that they are up to industry standards
                and are free of any issues. A couple of crucial skills are excellent time management and being able to regularly communicate,
                this helps when making documentation so that any found issues can be quickly and easily resolved. Including a FAQ or Q&A page could be
                one way to show these scenarios."
                pageContent24="While the most common programs tested are gaming systems and mobile applications because they need a lot of testing and maintenance,
                there's really no specific field that a Tester is confined to. They are often needed for lots of different programs,
                so being knowledgeable in a variety of systems can be beneficial. If you are aiming at a very specific company that specifializes
                in one type of system, having a lot of knowledge and experience with that system can also be valuable."
            />
            <Footer />
        </div>
    )
}

export default Examples;