import React from "react";
import Header from "../Header/header";
import NavigationBar from "../Navigation Bar/NavigationBar";
import PageBody from "../Page Body/PageBody";
import Footer from "../Footer/Footer";

const Homepage = () => {
    return (
        <div className="homepage-container">
            <Header />
            <NavigationBar />
            <PageBody
                bannerContainer
                title="Discover A Better Way to Picture Your Career"
                homeContent
                imageContent
                description="Portfolio Helper provides guidance on how to exhibit a history
                    of your skills and talent. It will guide you thorugh the process
                    of how to create and organize a personalized selection of your
                    experiences and expertise. Whether you are a grad student looking
                    for your first job, or an experienced professional looking
                    for new horizons, learning how to create an eye-catching portfolio
                    will set you apart from the rest and make a great first impression." />
            <Footer />
        </div>
    )
}

export default Homepage;