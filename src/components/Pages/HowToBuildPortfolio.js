import React from "react";
import Header from "../Header/header"
import NavigationBar from "../Navigation Bar/NavigationBar";
import PageBody from "../Page Body/PageBody";
import Footer from "../Footer/Footer";

const HowToBuildPortfolio = () => {
    return (
        <div className="HowToBuildPortfolio-container">
            <Header />
            <NavigationBar style={{ background: "#E9CCB7" }} />
            <PageBody style={{ backgroundImage: "none" }}
                howToBuildPortfolio
                howToContentLeft
                TitleBuild="HOW TO BUILD PORTFOLIO"

                pageContent4="Although it is a good practice to adjust your e-portfolio to the particular career field you're pursuing, there are some general rules that can apply for every e-portfolio. "

                pageContentTitle1="User-friendly Navigation"
                pageContent5="It is important to think about your audience and what you want to present to them. Don't overcomplicate it, make your e-portfolio easy to navigate. Make sure that you include your best projects in a well-organized format."

                pageContentTitle2="Include Description"
                pageContent6="A well-written description help create depth to visuals. Feel free to include personal stories that tell your journey, especially those that demonstrate your skills."

                pageContentTitle3="Include Biography"
                pageContent7="You can create an “About me” section on your e-portfolio and provide a brief description about you."

                pageContentTitle4="Career Goals"
                pageContent8="Display a summary of your professional goals and aspirations to illustrate employers about your ambitions. This will give them an idea about where do you see yourself in the future."

                pageContentTitle5="Resume"
                pageContent9="Include a copy of your resume and contact information in your e-portfolio."

                pageContentTitle6="Show your work"
                pageContent10="Include the most relevant and important work samples. This should represent a proof of your skills. Your work samples may include photos, artwork, articles, mockups, etc."

                pageContentTitle7="Networking"
                pageContent11="Include the link to your e-portfolio on your social media pages, such as LinkedIn to expand networking. "

                pageContentTitle8="Professional Summary"
                pageContent12="Compose a professional summary to describe what are your skills, motivation and goals."

                howToContentRight
                portfolioImage1

                bulletedListBox2
                sectionTitle2="E-portfolio website builders"
                bulletedListItem111="Squarespace"
                bulletedListItem222="Wix"
                bulletedListItem333="GoDaddy"
                bulletedListItem444="Duda"
                bulletedListItem555="Zyro"
                bulletedListItem666="Weebly"
                bulletedListItem777="WordPress"

                bulletedListBox1
                sectionTitle1="Tips"
                bulletedListItem8="Keep it simple"
                bulletedListItem9="Think like employer"
                bulletedListItem10="List your accomplishments and awards"
                bulletedListItem11="Think about what to show on your e-portfolio"
                bulletedListItem12="Show your professional goals"
            />
            <Footer />
        </div>
    )
}

export default HowToBuildPortfolio;