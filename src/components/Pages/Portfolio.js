import React from "react";
import Header from "../Header/header"
import NavigationBar from "../Navigation Bar/NavigationBar";
import PageBody from "../Page Body/PageBody";
import Footer from "../Footer/Footer";

const Portfolio = () => {
    return (
        <div className="portfolio-container">
            <Header />
            <NavigationBar style={{ background: "#E9CCB7" }} />
            <PageBody style={{ backgroundImage: "none" }}
                whatEportfolio
                portfolioLeftContent
                portfolioTitle="WHAT IS E-PORTFOLIO?"

                pageContent="E-Portfolio is a collection of your experience, skills, accomplishments, and traits, 
                and interests to be showcased in an interview. E-Portfolios are not just used in an academic or 
                professional setting, but have become a requirement for web developers, UX/UI designers, artists, 
                photographers, etc."
                pageContent1="A Portfolio's purpose is to show an interviewer who you are as a person. It shows them your personal interests,
                along with what school, work, and life experience you have. The idea is for it to give someone an understanding 
                of who you are in an organized and appealing manner."
                pageContent2="Your Portfolio should include items such as reference letters, a resume, some accomplishments that you are proud of, 
                samples of work that you have done, pictures to show as examples, relevant licenses or certifications, and evidence of 
                any important skillsets. More specific items can be added depending on the field of work or school, in order to show 
                that you have some knowledge in the field."
                pageContent3="A good e-portfolio represents a product or a digital collection of your work and a process that reflects these artifacts and what they represent.
                E-portfolio is NOT a placeholder of all your work. It is an electronic collection of evidence that shows your learning and professional journey."

                portfolioRightContent
                portfolioImage

                bulletedListBox
                sectionTitle="Portfolio Items"
                bulletedListItem1="Reference Letters"
                bulletedListItem2="Resume"
                bulletedListItem3="Accomplishments"
                bulletedListItem4="Samples of Work"
                bulletedListItem5="Photographs"
                bulletedListItem6="Licenses or Certifications"
                bulletedListItem7="Evidence of Skills"
            />
            <Footer />
        </div>
    )
}

export default Portfolio;