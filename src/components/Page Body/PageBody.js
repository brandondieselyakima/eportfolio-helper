import React from 'react';
import GoogleForm from '../GoogleForm/GoogleForm';


import "./PageBody.css"

const PageBody = (props) => {
    const {
        bannerContainer,
        bulletedListBox,
        bulletedListBox1,
        bulletedListBox2,
        bulletedListBox3,
        bulletedListBox4,
        bulletedListItem1,
        bulletedListItem10,
        bulletedListItem11,
        bulletedListItem111,
        bulletedListItem12,
        bulletedListItem2,
        bulletedListItem222,
        bulletedListItem3,
        bulletedListItem333,
        bulletedListItem4,
        bulletedListItem444,
        bulletedListItem5,
        bulletedListItem555,
        bulletedListItem6,
        bulletedListItem666,
        bulletedListItem7,
        bulletedListItem777,
        bulletedListItem8,
        bulletedListItem9,
        bulletedListItemEx1,
        bulletedListItemEx2,
        bulletedListItemEx3,
        bulletedListItemEx4,
        bulletedListItemEx5,
        bulletedListItemEx6,
        bulletedListItemEx7,
        bulletedListItemEx8,
        bulletedListItemEx9,
        description,
        engineerTitle,
        exampleMainPicture,
        examplesBodyContainer,
        examplesBottomContainer,
        examplesContent,
        examplesLeftContainer,
        examplesRightContainer,
        form,
        homeContent,
        howToBuildPortfolio,
        howToContentLeft,
        howToContentRight,
        imageContent,
        imgContainer1,
        imgContainer2,
        imgContainer3,
        imgContainer4,
        name1,
        name2,
        name3,
        name4,
        pageContent,
        pageContent1,
        pageContent10,
        pageContent11,
        pageContent12,
        pageContent13,
        pageContent14,
        pageContent15,
        pageContent16,
        pageContent18,
        pageContent19,
        pageContent2,
        pageContent20,
        pageContent21,
        pageContent22,
        pageContent23,
        pageContent24,
        pageContent3,
        pageContent4,
        pageContent5,
        pageContent6,
        pageContent7,
        pageContent8,
        pageContent9,
        pageContentTitle1,
        pageContentTitle2,
        pageContentTitle3,
        pageContentTitle4,
        pageContentTitle5,
        pageContentTitle6,
        pageContentTitle7,
        pageContentTitle8,
        portfolioAnna1,
        portfolioAnna2,
        portfolioAnna3,
        portfolioBA,
        portfolioBA1,
        portfolioBA2,
        portfolioDS1,
        portfolioDS2,
        portfolioImage,
        portfolioImage1,
        portfolioImage2,
        portfoliojm,
        portfoliojm1,
        portfoliojm2,
        portfolioLeftContent,
        portfolioRightContent,
        portfolioPictureBox1,
        portfolioPictureBox2,
        portfolioPictureBox3,
        portfolioPictureBox4,
        portfolioPictureBox5,
        portfolioPictureBox6,
        portfolioPictureBox7,
        portfolioPictureBox8,
        portfolioPictureBox9,
        portfolioPictureBox10,
        portfolioPictureBox11,
        portfolioTitle,
        portraitContainer,
        portraitImage1,
        portraitImage2,
        portraitImage3,
        portraitImage4,
        qatTitle,
        sectionTitle,
        sectionTitle1,
        sectionTitle2,
        sectionTitle3,
        sectionTitle4,
        style,
        title,
        TitleBuild,
        TitleExamples,
        UXTitle,
        whatEportfolio,
        Searchbox } = props;

    return (
        /* Page Body - Home Page */
        <div className="body-container" style={style} title="https://cdn.wallpapersafari.com/96/84/pdEZfz.jpg">
            {bannerContainer && <div className="banner-container">
                {title && <div className="title">{title}</div>}
                {homeContent && <div className="home-content-container">
                    {imageContent && <div className="image-content" alt="https://nextbigwhat.com/">{imageContent}</div>}
                    {description && <div className="description-box">{description}</div>}
                </div>}
            </div>}

            {/* Page Body - What's E-Portfolio Page */}
            {whatEportfolio && <div className="what-eportfolio-container">
                {portfolioLeftContent && <div className="portfolio-left-container">
                    {portfolioTitle && <div className="portfolio-title">{portfolioTitle}</div>}
                    {pageContent && <div className="page-content">{pageContent}</div>}
                    {pageContent1 && <div className="page-content1">{pageContent1}</div>}
                    {pageContent2 && <div className="page-content2">{pageContent2}</div>}
                    {pageContent3 && <div className="page-content3">{pageContent3}</div>}
                </div>}
                {portfolioRightContent && <div className="portfolio-right-container">
                    {portfolioImage && <div className="portfolio-image" alt="https://stock.adobe.com/"></div>}
                    {bulletedListBox && <div className="bulleted-list-box">
                        <div className="section-title">{sectionTitle}</div>
                        <div className="bulleted-list">
                            <li>{bulletedListItem1}</li>
                            <li>{bulletedListItem2}</li>
                            <li>{bulletedListItem3}</li>
                            <li>{bulletedListItem4}</li>
                            <li>{bulletedListItem5}</li>
                            <li>{bulletedListItem6}</li>
                            <li>{bulletedListItem7}</li>
                        </div>
                    </div>}
                </div>}
            </div>}

            {/* Page Body - How To Build Portfolio Page */}
            {howToBuildPortfolio && <div className="how-to-container">
                {howToContentLeft && <div className="how-to-left">
                    {TitleBuild && <div className="title-build">{TitleBuild}</div>}
                    {pageContent4 && <div className="page-content4">{pageContent4}</div>}
                    {pageContentTitle1 && <div className="page-title1">{pageContentTitle1}</div>}
                    {pageContent5 && <div className="page-content5">{pageContent5}</div>}
                    {pageContentTitle2 && <div className="page-title2">{pageContentTitle2}</div>}
                    {pageContent6 && <div className="page-content6">{pageContent6}</div>}
                    {pageContentTitle3 && <div className="page-title3">{pageContentTitle3}</div>}
                    {pageContent7 && <div className="page-content7">{pageContent7}</div>}
                    {pageContentTitle4 && <div className="page-title4">{pageContentTitle4}</div>}
                    {pageContent8 && <div className="page-content8">{pageContent8}</div>}
                    {pageContentTitle5 && <div className="page-title5">{pageContentTitle5}</div>}
                    {pageContent9 && <div className="page-content9">{pageContent9}</div>}
                    {pageContentTitle6 && <div className="page-title6">{pageContentTitle6}</div>}
                    {pageContent10 && <div className="page-content10">{pageContent10}</div>}
                    {pageContentTitle7 && <div className="page-title7">{pageContentTitle7}</div>}
                    {pageContent11 && <div className="page-content11">{pageContent11}</div>}
                    {pageContentTitle8 && <div className="page-title8">{pageContentTitle8}</div>}
                    {pageContent12 && <div className="page-content12">{pageContent12}</div>}
                </div>}
                {howToContentRight && <div className="how-to-right">
                    {portfolioImage1 && <div className="portfolio-image1" alt="https://www.costainvest.org/portfolio-web-inmobiliaria/"></div>}
                    {bulletedListBox1 && <div className="bulleted-list-box1">
                        <div className="section-title">{sectionTitle1}</div>
                        <div className="bulleted-list">
                            <li>{bulletedListItem8}</li>
                            <li>{bulletedListItem9}</li>
                            <li>{bulletedListItem10}</li>
                            <li>{bulletedListItem11}</li>
                            <li>{bulletedListItem12}</li>
                        </div>
                    </div>}
                    {bulletedListBox2 && <div className="bulleted-list-box2">
                        <div className="section-title">{sectionTitle2}</div>
                        <div className="bulleted-list">
                            <li>{bulletedListItem111}</li>
                            <li>{bulletedListItem222}</li>
                            <li>{bulletedListItem333}</li>
                            <li>{bulletedListItem444}</li>
                            <li>{bulletedListItem555}</li>
                            <li>{bulletedListItem666}</li>
                            <li>{bulletedListItem777}</li>
                        </div>
                    </div>}
                </div>}
            </div>}

            {/* Examples Body Container - Examples Page */}
            {examplesBodyContainer && <div className="examples-body">
                {/* Examples Bottom Container - Examples Page */}
                {examplesBottomContainer && <div className="examples-bottom-container">
                    {/* Left Column - Examples Page */}
                    {examplesLeftContainer && <div className="examples-left-container">
                        {examplesContent && <div className="examples-content">
                            {TitleExamples && <div className="title-examples">{TitleExamples}</div>}
                            {pageContent13 && <div className="page-content13">{pageContent13}</div>}
                            {pageContent14 && <div className="page-content14">{pageContent14}</div>}
                        </div>}
                        {UXTitle && <div className="ux-title">{UXTitle}</div>}
                        {pageContent15 && <div className="page-content15">{pageContent15}</div>}
                        {pageContent16 && <div className="page-content16">{pageContent16}</div>}
                        {pageContent18 && <div className="page-content18">{pageContent18}</div>}
                        {pageContent19 && <div className="page-content19">{pageContent19}</div>}
                        {pageContent20 && <div className="page-content20">{pageContent20}</div>}

                        {bulletedListBox3 && <div className="bulleted-list-box3">
                            <div className="section-title3">{sectionTitle3}</div>
                            <div className="bulleted-list">
                                <li>{bulletedListItemEx1}</li>
                                <li>{bulletedListItemEx2}</li>
                                <li>{bulletedListItemEx3}</li>
                                <li>{bulletedListItemEx4}</li>

                            </div>
                        </div>}

                        {bulletedListBox4 && <div className="bulleted-list-box4">
                            <div className="section-title4">{sectionTitle4}</div>
                            <div className="bulleted-list">
                                <li>{bulletedListItemEx5}</li>
                                <li>{bulletedListItemEx6}</li>
                                <li>{bulletedListItemEx7}</li>
                                <li>{bulletedListItemEx8}</li>
                                <li>{bulletedListItemEx9}</li>
                            </div>
                        </div>}
                        {/*Software Engineer Content*/}
                        {engineerTitle && <div className="engineer-title">{engineerTitle}</div>}
                        {pageContent21 && <div className="page-content21">{pageContent21}</div>}
                        {pageContent22 && <div className="page-content22">{pageContent22}</div>}

                        {qatTitle && <div className="qat-title">{qatTitle}</div>}
                        {pageContent23 && <div className="page-content23">{pageContent23}</div>}
                        {pageContent24 && <div className="page-content24">{pageContent24}</div>}

                    </div>}

                    {/* Right Column - Examples Page */}
                    {examplesRightContainer && <div className="examples-right-container">
                        {exampleMainPicture && <div className="examples-main-picture">
                            {portfolioImage2 && <div className="portfolio-image2" alt="https://www.istockphoto.com/search/2/image?phrase=job+search"></div>}
                        </div>}
                        {portfolioPictureBox1 && <div className="portfolio-picture-box1">
                            {portfolioAnna1 && <div className="portfolio-anna1" title="https://stock.adobe.com/"></div>}
                        </div>}
                        {portfolioPictureBox2 && <div className="portfolio-picture-box2">
                            {portfolioAnna2 && <div className="portfolio-anna2" title="https://stock.adobe.com/"></div>}
                        </div>}
                        {portfolioPictureBox3 && <div className="portfolio-picture-box3">
                            {portfolioAnna3 && <div className="portfolio-anna3" title="https://stock.adobe.com/"></div>}
                        </div>}
                        {portfolioPictureBox4 && <div className="portfolio-picture-box4">
                            {portfoliojm && <div className="portfolio-jm" title="https://stock.adobe.com/"></div>}
                        </div>}
                        {portfolioPictureBox5 && <div className="portfolio-picture-box5">
                            {portfoliojm1 && <div className="portfolio-jm1" title="https://stock.adobe.com/"></div>}
                        </div>}
                        {portfolioPictureBox6 && <div className="portfolio-picture-box6">
                            {portfoliojm2 && <div className="portfolio-jm2" title="https://stock.adobe.com/"></div>}
                        </div>}
                        {portfolioPictureBox7 && <div className="portfolio-picture-box7">
                            {portfolioBA && <div className="portfolio-ba"></div>}
                        </div>}
                        {portfolioPictureBox8 && <div className="portfolio-picture-box8">
                            {portfolioBA1 && <div className="portfolio-ba1" title="https://www.mytemplatestorage.com/blog/tips-and-tricks/principles-of-web-design/"></div>}
                        </div>}
                        {portfolioPictureBox9 && <div className="portfolio-picture-box9">
                            {portfolioBA2 && <div className="portfolio-ba2" title="https://pixabay.com/illustrations/resume-bio-data-job-employment-1799954/"></div>}
                        </div>}
                        {portfolioPictureBox10 && <div className="portfolio-picture-box10">
                            {portfolioDS1 && <div className="portfolio-DS1"></div>}
                        </div>}
                        {portfolioPictureBox11 && <div className="portfolio-picture-box11">
                            {portfolioDS2 && <div className="portfolio-DS2"></div>}
                        </div>}
                    </div>}
                </div>}
            </div>}

            {/* Page Body - Our Team */}
            {portraitContainer && <div className="portrait-container">
                <div className="section-title2">{sectionTitle2}</div>
                <div className="portrait-image-box">
                    {imgContainer1 && <div className="portrait-img-container1">
                        <div className="portrait-image1">{portraitImage1}</div>
                        {name1 && <div className="name1">{name1}</div>}
                    </div>}
                    {imgContainer2 && <div className="portrait-img-container2">
                        <div className="portrait-image2">{portraitImage2}</div>
                        {name2 && <div className="name2">{name2}</div>}
                    </div>}
                    {imgContainer3 && <div className="portrait-img-container3">
                        <div className="portrait-image3">{portraitImage3}</div>
                        {name3 && <div className="name3">{name3}</div>}
                    </div>}
                    {imgContainer4 && <div className="portrait-img-container4">
                        <div className="portrait-image4">{portraitImage4}</div>
                        {name4 && <div className="name4">{name4}</div>}
                    </div>}
                </div>
            </div>}

            {/* Google Form Embed - Contact Us Page */}
            {form && <div className="form-embed"><GoogleForm /></div>}
            {Searchbox && <div className="search"><Searchbox /></div>}
          
        </div>
        
    )
}

export default PageBody;