import React, { useState } from "react";

import "./Searchbox.css";

function Searchbox(props) {
    const {
        icon,
    } = props;

    const [searchInput, setSearchInput] = useState("");

    const handleClick = (e) => {
        e.preventDefault();
        const url = "https://www.google.com/search?q=" + searchInput;
        window.open(url);
        setSearchInput("");
    }

    return (
        <div className="search-container">
            <div className="search-input">
                <input type="text"
                    placeholder="Search"
                    value={searchInput}
                    onChange={e => setSearchInput(e.target.value)}
                    required />
                <div className="search-button">
                    <i className="search-icon" onClick={handleClick}>{icon}</i>
                </div>
            </div>
        </div>
    );
};

export default Searchbox;