import React from "react";

import "./Footer.css"

const Footer = () => {
    return (
        <div className="footer-container">
            <div className="copyright-container">Team 1 CSIT 411 ©Copyright 2022</div>
        </div>
    )
}

export default Footer;