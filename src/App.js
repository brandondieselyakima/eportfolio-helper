import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Homepage from "./components/Pages/Homepage";
import Portfolio from "./components/Pages/Portfolio";
import Contact from "./components/Pages/Contact";
import OurTeam from "./components/Pages/OurTeam";
import Examples from "./components/Pages/Examples";
import HowToBuildPortfolio from "./components/Pages/HowToBuildPortfolio";

import "./App.css";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/eportfolio-helper" element={<Homepage />} />
        <Route path="OurTeam" element={<OurTeam />} />
        <Route path="portfolio" element={<Portfolio />} />
        <Route path="contact" element={<Contact />} />
        <Route path="HowToBuildPortfolio" element={<HowToBuildPortfolio />} />
        <Route path="Examples" element={<Examples />} />

        <Route path="*" element={<p>Not Found</p>} />
      </Routes>
    </Router>
  );
}

export default App;